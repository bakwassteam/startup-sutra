import logging

from flask import Flask
from flask.ext.cors import CORS

from controller import controller_api

app = Flask(__name__)

# Initializing the controller API
controller_api.init_app(app)
CORS(app)

def main():
    logging.basicConfig(level=logging.DEBUG,filename='startupsutra.log',
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(relativeCreated)6d %(threadName)s %(message)s',
                    datefmt='%m-%d %H:%M')
    logging.info("Starting the App.")
    app.run()
    logging.info("Stopped running App.")

if __name__ == '__main__':
    main()

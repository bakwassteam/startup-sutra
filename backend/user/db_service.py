from backend.user import marshaller
from backend.util import DbUtil
import logging as log

USER_EMAIL = 'user_email'

USER_COLLECTION_NAME = 'USERS'
STARTUP_SUTRA = 'STARTUP_SUTRA'


class DbService:
    # Self variables
    def __init__(self):
        self.user = None
        self.users_list = []
        log.debug('Creating dbService')
        self.db = DbUtil()
        self.database = self.db.create_database(STARTUP_SUTRA)
        self.collection = self.db.create_collection(USER_COLLECTION_NAME)
        self.db.create_index([USER_EMAIL], USER_COLLECTION_NAME)

    def create_user(self, user):
        try:
            ''' Write business validations '''
            log.debug('Marshalling data to mongo format')
            # Marshal the user object
            user_json_format = marshaller.get_user_collection_model(user)

            log.debug('Inserting data into MongoDB collection %s ' % USER_COLLECTION_NAME)
            # Insert the user object to USERS table
            collection_db_id = self.collection.get(USER_COLLECTION_NAME) \
                .insert_one(user_json_format) \
                .inserted_id

            return user

        except Exception as e:
            log.error(str(e))

    def authenticate_user(self, user_password, user_email):
        try:
            authenticated_user = self.collection[USER_COLLECTION_NAME].find_one({
                "$and": [
                        {"user_email": user_email},
                        {"user_password": user_password}
                    ]
            })
            if not authenticated_user:
                raise Exception('User is not authenticated')
            return authenticated_user
        except Exception as e:
            log.error(str(e))

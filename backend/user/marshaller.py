__author__ = 'Anand Shubham Gorthi'


def get_user_collection_model(user):
    user_collection_model = {
        "user_email": user.user_email,
        "user_password": user.user_password,
        "user_name": user.user_name,
        "user_extra_details": user.user_extra_details,
        "user_followers": user.user_followers,
        "user_following": user.user_following,
        "user_article_posts": user.user_article_posts,
        "user_commented_posts": user.user_commented_posts,
        "user_liked_posts": user.user_commented_posts,
        "user_disliked_posts": user.user_disliked_posts,
        "user_forum_posts": user.user_forum_posts,
    }
    return user_collection_model
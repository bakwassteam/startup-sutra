import logging as log

import pymongo
from pymongo import MongoClient

# This would make us to create only one connection
# instead of every time creating a new one
db_client = MongoClient('mongodb://localhost:27017/')

# Class for all Db utilities services
class DbUtil():
    # db variables
    def __init__(self):
        # each service can use different database
        self.database = None
        # each service can use list of collections
        self.collection = {}

    """
        Create a database in mongoDb and return the
        database. It is an object variable, so each
        db_service will have its own database
        @:returns database for the service
    """

    def create_database(self, database_name):
        log.debug('Trying to create database with %s ' % database_name)
        try:
            self.database = db_client[database_name]
        except Exception as e:
            log.error(str(e))
        log.debug('Successfully created database %s ' % database_name)
        return self.database

    '''
        Creates (or) gets a collection
        @:returns dictionary of collections for the service
    '''

    def create_collection(self, collection_name):
        log.debug('Trying to create a collection %s ' % collection_name)
        try:
            self.collection[collection_name] = self.database[collection_name]
        except Exception as e:
            log.error(str(e))
        log.debug('Successfully created collection %s ' % collection_name)
        return self.collection

    '''
        Creates an index on a specified collection
        on a specified list of attributes
    '''

    def create_index(self, list_of_attributes, collection_name):
        collection = self.collection[collection_name]
        try:
            for attribute in list_of_attributes:
                log.debug('Creating index on %s attribute of %s collection ' % (attribute, collection_name))
                collection.create_index([(attribute, pymongo.ASCENDING)], unique=True)
        except Exception as e:
            log.error(str(e))

    """
        Getters
    """

    def get_database(self):
        return self.database

    def get_collection(self):
        return self.collection

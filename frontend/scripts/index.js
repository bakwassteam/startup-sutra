var app = angular.module('StartupSutraIndex', ["ngRoute"]);
       app.controller('StartupSutraAuthentication', function($scope, $http, $window){
            $scope.authenticateUser = function() {
                var dataToSend = {
                    "user_email" : $scope.user_email,
                    "user_password" : $scope.user_password
                };
                $http.post("http://localhost:5000/user/authenticate", dataToSend)
                    .then(function(response) {
                        if (response != null) {
                            if (response.data.user_email != null) {
                                $window.location.href = './home.html';
                            }
                        }
                    },
                    function(error) {
                        console.log("error -> " + error);
                    }
                )
            }
    });
var app = angular.module("StartupSutraRegister",[]);
    app.controller("StartupSutraRegisterCreateController", function ($scope, $http) {
        $scope.createUser = function () {
            var educationDetails = [];
            var professionDetails = [];
            var interestDetails = [];
            var languageDetails = [];

            var educations = $scope.educations;
            var professions = $scope.professions;
            var interests = $scope.interests;
            var languages = $scope.languages;

            if (educations != undefined)
            educationDetails = educations.trim().split(",");
            if (professions != undefined)
            professionDetails = professions.trim().split(",");
            if (interests != undefined)
            interestDetails = interests.trim().split(",");
            if (languages != undefined)
            languageDetails = languages.trim().split(",");

            var dataToSend = {
                "user_name" : $scope.user_name,
                "user_email" : $scope.user_email,
                "user_password" : $scope.user_password,
                "user_extra_details" : {
                    "education" : educationDetails,
                    "interests" : interestDetails,
                    "profession" : professionDetails,
                    "languages" : languageDetails
                }
            };
            console.log(dataToSend);
            $http.post("http://localhost:5000/user/create", dataToSend)
                .then(function (response) {
                    console.log(response.data.user_name);
                    console.log(response.data.user_email);
                }, function(error) {

            })
        }
    });
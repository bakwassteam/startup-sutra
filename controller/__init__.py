# Controller module consisting of all the endpoints

from flask_restplus import Api

from controller.user_endpoints.user_controller import user_api as user_endpoint_namespace


controller_api = Api()

# Adding Namespaces
controller_api.add_namespace(user_endpoint_namespace)

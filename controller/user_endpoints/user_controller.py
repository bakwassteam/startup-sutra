from flask_restplus import Resource, Namespace, fields
from domain.user.session_management import authenticated_users

from domain.user.user_domain import UserDomain


user_domain = UserDomain()
user_api = Namespace('user', description='Endpoints for user related operations')
user_model = user_api.model('create_user', {
    'user_name': fields.String(required=True, description='User name'),
    'user_email': fields.String(description='Email')
})


"""
    POST route for creating a user from a form
"""


@user_api.route('/create')
@user_api.response(404, 'User cannot be created')
class CreateUser(Resource):
    @user_api.marshal_list_with(user_model)
    def post(self):
        return user_domain.create_user()


"""
    POST route for authenticating a user from a form
    it also creates a session for user
"""


@user_api.route('/authenticate')
@user_api.response(404, 'User is not authenticated')
class AuthenticateUser(Resource):
    @user_api.marshal_list_with(user_model)
    def post(self):
        return user_domain.authenticate_user()

# ----------- NEED TO DELETE THIS METHOD ------------- #


@user_api.route('/get_session')
class GetSession(Resource):
    def get(self):
        return authenticated_users

"""
    GET route for logout
    removes the session for the authenticated user

"""


@user_api.route('/logout/<user_email>')
class Logout(Resource):
    @user_api.marshal_with(user_model)
    def get(self, user_email):
        return user_domain.logout(user_email)

authenticated_users = []


def create_session(user_email):
    if user_email is '' or user_email in authenticated_users:
        raise Exception('Session already existing')
    else:
        authenticated_users.append(user_email)


def delete_session(user_email):
    try:
        if user_email in authenticated_users:
            authenticated_users.remove(user_email)
        else:
            raise Exception('User do not exist')
    except:
        raise Exception('Session cannot be deleted')


def is_already_authenticated(user_email):
        return user_email in authenticated_users

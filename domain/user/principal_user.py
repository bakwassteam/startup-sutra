# User object
class PrincipalUser:

    # self variables
    def __init__(self):
        # contains the user_identifier
        self.user_name = ''
        self.user_email = ''
        # contains the hashed password
        self.user_password = ''
        # contains the user_ids following the user
        self.user_followers = []
        # contains the user_ids the user is following
        self.user_following = []
        # contains the article_post ids
        self.user_article_posts = []
        # contains the user_commented object ids
        self.user_commented_posts = []
        # contains the user_liked posts ids
        self.user_liked_posts = []
        # contains the user_disliked posts ids
        self.user_disliked_posts = []
        # contains forum_post ids
        self.user_forum_posts = []
        # contains a dictionary of extra details
        self.user_extra_details = {}

# Class for all user related operations from endpoint
from flask_restplus import reqparse

from backend.user.db_service import DbService
from domain import domain_util
from domain.user.principal_user import PrincipalUser
from domain.user.session_management import *


USER_EXTRA_DETAILS = 'user_extra_details'
PASSWORD = 'user_password'
EMAIL = 'user_email'
USER_NAME = 'user_name'


class UserDomain:
    # Self variables
    def __init__(self):
        self.db_service = DbService()

    def authenticate_user(self):
        parser = reqparse.RequestParser()
        parser.add_argument(PASSWORD, location='json')
        parser.add_argument(EMAIL, location='json')
        user_attr_dict = parser.parse_args()

        user_email = user_attr_dict[EMAIL]
        # Authenticating user only if he/she is not already authenticated
        if not is_already_authenticated(user_email):
            user_password = domain_util.encrypt_data(user_attr_dict[PASSWORD])
            authenticated_user = self.db_service.authenticate_user(user_password, user_email)
            if authenticated_user:
                create_session(user_email)
                return authenticated_user
        raise Exception('User already authenticated')

    def create_user(self):
        parser = reqparse.RequestParser()

        parser.add_argument(USER_NAME, type=str, location='json', required=True)
        parser.add_argument(EMAIL, type=str, location='json', required=True)
        parser.add_argument(PASSWORD, type=str, location='json', required=True)
        parser.add_argument(USER_EXTRA_DETAILS, type=dict, location='json', required=True)

        user_attr_dict = parser.parse_args()

        user = PrincipalUser()
        user.user_name = user_attr_dict[USER_NAME]
        user.user_email = user_attr_dict[EMAIL]
        user.user_password = domain_util.encrypt_data(user_attr_dict[PASSWORD])
        user.user_extra_details = user_attr_dict[USER_EXTRA_DETAILS]

        return self.db_service.create_user(user)

    def logout(self, user_email):
        delete_session(user_email)
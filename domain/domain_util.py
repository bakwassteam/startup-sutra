import hashlib
import logging as log
__author__ = 'Anand Shubham Gorthi'

''' File for performing all utility functions '''


def encrypt_data(data):
    hasher = hashlib.md5()
    try:
        hasher.update(str.encode(data))
    except Exception as e:
        log.error(str(e))
    return hasher.digest()




